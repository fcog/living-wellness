<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie-7"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie-8"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <meta http-equiv="cleartype" content="on">

    <!-- Responsive and mobile friendly stuff -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.jpg">

    <!--jQuery-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.8.2.min.js"></script>

    <!--UI theme roller-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/custom-theme/jquery-ui-1.8.24.custom.css" />

    <!--Styles-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style.css" />

    <!-- Responsive Stylesheets -->
    <link rel="stylesheet" media="only screen and (max-width: 1024px) and (min-width: 769px)" href="<?php echo get_template_directory_uri(); ?>/css/1024.css">
    <link rel="stylesheet" media="only screen and (max-width: 768px) and (min-width: 481px)" href="<?php echo get_template_directory_uri(); ?>/css/768.css">
    <link rel="stylesheet" media="only screen and (max-width: 480px)" href="<?php echo get_template_directory_uri(); ?>/css/480.css">    

    <!--prefix free-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.5.3-min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>

    <!--Only Mac - Safari Class-->
    <script type="text/javascript">
    jQuery(function(){
        // console.log(navigator.userAgent);
        /* Adjustments for Safari on Mac */
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1) {
            jQuery('html').addClass('mac'); // provide a class for the safari-mac specific css to filter with
        }
    });
    </script>

    <!--Selectivizr-->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/selectivizr.js"></script>
        <noscript><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/theme.css" /></noscript>
    <![endif]-->

    <!--[if IE]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if gte IE 9]>
        <style type="text/css">
         .gradient {
            filter: none;
        }
     </style>
    <![endif]-->

	<title><?php echo get_bloginfo("name"); ?></title>
<?php wp_head(); ?>
</head>
<body>
<div id="wrapper">
    <header id="header">
        <div class="inner">
            <h1>
                <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/photos/logo.png"></a>
            </h1>
            <div id="contact-info">
                <div class="textwidget">
                    <?php
                    if (is_active_sidebar('homepage-phone-number')) :
                        dynamic_sidebar('homepage-phone-number');
                    endif;
                    ?>                     
                </div>
                    <ul id="social-network">
                    <?php
                    if (is_active_sidebar('homepage-social-buttons')) :
                        dynamic_sidebar('homepage-social-buttons');
                    endif;
                    ?>                    
                </ul>
                <a href="<?php echo home_url(); ?>/request-free-consultation" class="button">Request a free consultation!</a>
            </div>
        </div>
    </header>
    <div id="main-nav">
        <nav class="inner">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
        </nav>
    </div>
