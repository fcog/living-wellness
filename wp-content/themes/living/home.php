<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

    <div id="slideshow" class="inner">
        <?php echo do_shortcode('[ngg_images gallery_ids="1" display_type="photocrati-nextgen_basic_slideshow"]') ?>
    </div>
    <div id="home-content">
        <div class="inner">
            <section id="main-content" class="col span_3_of_4">
                <?php
                   /*
                <div class="section group">
                     $args = array('posts_per_page' => 2,'post_type' => 'post');

                    $posts_array = get_posts( $args );

                    foreach ( $posts_array as $post ): 
                                     
                        <article class="col span_1_of_2">
                            <h2><a href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></h2>
                            <?php if (has_post_thumbnail()): ?><div class="img-news"><?php the_post_thumbnail(); ?></div><?php endif ?>
                            <?php the_excerpt(); ?>
                            <a href="<?php echo get_permalink(); ?>"></a>
                        </article>
                    endforeach; wp_reset_postdata(); 
                </div>
                */?>
                <div class="section group">
                    <div class="col span_1_of_2" id="content0">
                        <?php
                        if (is_active_sidebar('homepage-content0')) :
                            dynamic_sidebar('homepage-content0');
                        endif;
                        ?>  
                    </div>                     
                    <div class="col span_1_of_2" id="content1">
                        <?php
                        if (is_active_sidebar('homepage-content1')) :
                            dynamic_sidebar('homepage-content1');
                        endif;
                        ?>  
                    </div> 
                    <div class="col span_1_of_2" id="content2">
                        <?php
                        if (is_active_sidebar('homepage-content2')) :
                            dynamic_sidebar('homepage-content2');
                        endif;
                        ?>  
                    </div>
                </div>
                <div class="section group">
                    <div class="col span_1_of_2" id="content3">
                        <?php
                        if (is_active_sidebar('homepage-content3')) :
                            dynamic_sidebar('homepage-content3');
                        endif;
                        ?>  
                    </div> 
                    <div class="col span_1_of_2 ">
                        <?php
                        if (is_active_sidebar('homepage-content4')) :
                            dynamic_sidebar('homepage-content4');
                        endif;
                        ?>  
                    </div>
                </div>                                                           
            </section>
            <aside id="sidebar" class="col span_1_of_4">
                <div class="box heal">
                    <?php
                    if (is_active_sidebar('homepage-sidebar1')) :
                        dynamic_sidebar('homepage-sidebar1');
                    endif;
                    ?>  
                </div>
                <div class="box">
                    <?php
                    if (is_active_sidebar('homepage-sidebar2')) :
                        dynamic_sidebar('homepage-sidebar2');
                    endif;
                    ?>                     
                </div>
                <div class="box logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/photos/logo-aside.png">
                </div>
            </aside>
        </div>
    </div>


<?php get_footer(); ?>    