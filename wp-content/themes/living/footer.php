    <footer id="footer">
        <div id="info-footer" class="inner">
            <div class="col span_1_of_4 ">
                <?php
                if (is_active_sidebar('footer-facebook')) :
                    dynamic_sidebar('footer-facebook');
                endif;
                ?>                 
            </div>
            <div class="col span_1_of_4 ">
                <h2>Latest Products</h2>
                <?php
                $args = array('posts_per_page' => 4,'post_type' => 'products', 'order'=>'ASC');

                $posts_array = get_posts( $args );

                foreach ( $posts_array as $post ): 
                ?>  
                    <li><a href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></li>
                <?php endforeach; wp_reset_postdata(); ?>    
            </div>
            <div class="col span_1_of_4 ">
                <h2>Latest News</h2>
                <?php
                $args = array('posts_per_page' => 4);

                $posts_array = get_posts( $args );

                foreach ( $posts_array as $post ):
                ?>  
                    <li><a href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></li>
                <?php endforeach; wp_reset_postdata(); ?>   
            </div>
            <div class="col span_1_of_4 ">
                <?php
                if (is_active_sidebar('footer-directions')) :
                    dynamic_sidebar('footer-directions');
                endif;
                ?>                    
            </div>
        </div>
        <div id="copyright">
            <div class="inner">
                Copyright © 2013 Living Wellness Center.
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</div>
</body>