<?php

function living_setup() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'living' ) );
	register_nav_menu( 'footer', __( 'Footer Menu', 'living' ) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 370, 173, true );
}
add_action( 'after_setup_theme', 'living_setup' );

function add_menuclass($ulclass) {
	return preg_replace('/<a /', '<a class="span_1_of_8" ', $ulclass);
}
add_filter('wp_nav_menu','add_menuclass');

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function living_widgets_init() {

/************* HEADER *********************/
    register_sidebar( array(
      'id'            => 'homepage-phone-number',
      'name'          => __( 'Homepage - Phone number', 'living' ),
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'homepage-social-buttons',
      'name'          => __( 'Homepage - Social buttons', 'living' ),
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'homepage-content0',
      'name'          => __( 'Homepage - Mobile Header', 'living' ),
      'before_widget'  => '',                  
    ) );      

    register_sidebar( array(
      'id'            => 'homepage-content1',
      'name'          => __( 'Homepage - Content 1', 'living' ),
      'before_widget'  => '',                  
    ) );     

    register_sidebar( array(
      'id'            => 'homepage-content2',
      'name'          => __( 'Homepage - Content 2', 'living' ),
      'before_widget'  => '',                  
    ) );   

    register_sidebar( array(
      'id'            => 'homepage-content3',
      'name'          => __( 'Homepage - Content 3', 'living' ),
      'before_widget'  => '',                  
    ) );    
     
    register_sidebar( array(
      'id'            => 'homepage-content4',
      'name'          => __( 'Homepage - Content 4', 'living' ),
      'before_widget'  => '',                  
    ) );     

   register_sidebar( array(
      'id'            => 'homepage-sidebar1',
      'name'          => __( 'Homepage - Sidebar 1', 'living' ),
      'before_widget'  => '',                  
    ) );    

   register_sidebar( array(
      'id'            => 'homepage-sidebar2',
      'name'          => __( 'Homepage - Sidebar 2', 'living' ),
      'before_widget'  => '',                  
    ) );    

    register_sidebar( array(
      'id'            => 'footer-facebook',
      'name'          => __( 'Footer - Facebook', 'living' ),
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'footer-directions',
      'name'          => __( 'Footer - Directions', 'living' ),
      'before_widget'  => '',                  
    ) ); 

    register_sidebar( array(
      'id'            => 'sidebar',
      'name'          => __( 'Sidebar', 'living' ),
      'before_widget'  => '',                  
    ) );         
}
add_action( 'widgets_init', 'living_widgets_init' );

/************* CUSTOM WIDGETS *****************/


/************* SOCIAL LINKS *****************/

class SocialLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Social Link','description=Social Link');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'class' => '', 'link' => '' ) );
			    $class = $instance['class'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('class'); ?>">CSS Class: </label><input class="widefat" id="<?php echo $this->get_field_id('class'); ?>" name="<?php echo $this->get_field_name('class'); ?>" type="text" value="<?php echo attribute_escape($class); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['class'] = $new_instance['class'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $class = empty($instance['class']) ? '' : apply_filters('widget_title', $instance['class']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    if (!empty($link))
		      echo "<li class='".$class."'><a href='".$link."' target='_blank'>".$class."</a></li>";

		    echo $after_widget;
        }

}
register_widget( 'SocialLinkWidget' );

/************* HELPER FUNCTIONS *****************/

function html_widget_title( $title ) {
	//HTML tag opening/closing brackets
	$title = str_replace( '[', '<', $title );
	$title = str_replace( ']', '/>', $title );

	return $title;
}
add_filter( 'widget_title', 'html_widget_title' );