<?php
/*
Template Name: Products
*/
?>
<?php get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

	        <section id="news-events" class="grid_18">
	            <h1 class="entry-title">Products</h1>
	            <?php
	            $args = array('posts_per_page' => 20,'post_type' => 'products');

	            $posts_array = get_posts( $args );

	            foreach ( $posts_array as $post ): 
	            ?>
	            	<article>            
						<h2 class="entry-title-secondary"><?php the_title(); ?></h2>

						<?php the_excerpt(); ?>

						<a href="<?php echo get_permalink(); ?>">Read more &raquo;</a>
					</article>

	            <?php endforeach ?>
	        </section>

		</div><!-- #content -->

	</div><!-- #primary -->

<?php get_footer(); ?>