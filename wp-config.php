<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'livingwell_wp');

/** MySQL database username */
define('DB_USER', 'livingwell');

/** MySQL database password */
define('DB_PASSWORD', 'Hus7%3k7s%shy');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'RvW_^e#{-zM>Mg080+Ofc9,q^43qlwXXwGZf^OJoW5Zu84$,.D9}XvpB/XTSWG)j');
define('SECURE_AUTH_KEY',  '(HqPz0JZE;p+0gWSswiSN{1`P[7PEqVfU$v)KziX4Er#0W<{abwS9~~O-v=nJRD6');
define('LOGGED_IN_KEY',    'F#5lb1&_R6Se!jh|hgO-L2h`khJQGP5G|7&a>naw2K(eS$csRCqZcqm/JE?qw+Uf');
define('NONCE_KEY',        '(/kB-&k~ihdhk7Y:lw-XupYnY Z|,-G7JNa<Fu6z# L|AF+4[8+E#^<7Rr+A}=n#');
define('AUTH_SALT',        'VM6D%Ibr7eS2=ukuKD/D[D:,x^o!XD}uFBcK|<{f|wbvyk6wgv2cv W`-Hbf9zxo');
define('SECURE_AUTH_SALT', 'R&xw/ X)MknxhA`M%RV*vqozL#EZ[jZ8:e>cCl-aIBms5Bl`AqZ$SY%o2VPh+SBB');
define('LOGGED_IN_SALT',   'q8WwXJ^HJ>(j 43C[*OA!kdEpip?6h%QK_G(%qOIXV4J)-kT++R8}]>Q)[/}0OZx');
define('NONCE_SALT',       'hKT&!2/>r8}-8@OI%EXi(l#[PbS~pAzGTL({uG&q,wTNwj6BH9g%!~:o,xa&ELo7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
