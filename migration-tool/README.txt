Tips for usage:

migration_ms.php: Intended for multisites migration
migration_solo.php: Intended for Wordpress without multisite enabled

Linux:
- Create a symlink migration.php to the version you need: migration_ms.php / migration_solo.php

Windows:
- Rename the right file depending on your needs.

LAST UPDATED: May 25th/2011

